from django.urls import path
from . import views

app_name='gradebook'

urlpatterns = [
    path('student/<int:id>', views.student_detail, name='student_detail'),
    path('student/<int:id>/add', views.add_marks, name='add_marks'),
    path('student/<int:id>/delete', views.delete_marks, name='delete_marks'),
    path('login', views.login_to_session, name='login'),
    path('logout', views.logout_, name='logout')
]