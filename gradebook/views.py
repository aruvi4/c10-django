from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, permission_required
from .models import Student, Marks
# Create your views here.

@permission_required('gradebook.can_perform_magic', login_url='gradebook:login')
def student_detail(request, id):
    #this is the R of CRUD
    student_ = Student.objects.get(pk=id)
    #SELECT * FROM STUDENT WHERE <INSERT PRIMARY KEY HERE>=id
    marks = Marks.objects.filter(student=student_)
    #SELECT * FROM MARKS WHERE STUDENT=(SELECT * FROM STUDENT
    # WHERE <INSERT PRIMARY KEY HERE>=id)
    #is_authenticated = request.session.get('logged_in_student') == student_.user.username
    context = {'student': student_, 'marks': marks}
    #if is_authenticated:
        #context['authenticated_student_id'] = id
    return render(request, 'gradebook/marksheet.html', context)

def add_marks(request, id):
    #this is the C of CRUD
    student_ = Student.objects.get(pk=id)

    sub_name_ = request.POST.get('sub_name')
    score_ = request.POST.get('score')

    marks = Marks(student=student_, subject_name=sub_name_, score=score_)
    marks.save()
    return redirect('gradebook:student_detail', student_.id)

def delete_marks(request, id):
    #this is the D of crud
    marks = Marks.objects.get(pk=id)
    marks.delete()
    return redirect('gradebook:student_detail', marks.student.id)

def login_to_session(request):
    error_message = ''
    if request.method == 'POST':
        username_ = request.POST.get('username')
        password_ = request.POST.get('password')
        user = authenticate(username=username_, password=password_)
        if user is not None:
            login(request, user)
            student = Student.objects.get(user=user)
            return redirect('gradebook:student_detail', student.id)
        else:
            error_message = 'Wrong password'
    context = {'error_message': error_message}
    return render(request, 'gradebook/login.html', context)

def logout_(request):
    logout(request)
    return redirect('gradebook:login')
