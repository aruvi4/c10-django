1. CREATE YOUR APP

django-admin startapp `<app_name>`

2. REGISTER YOUR APP IN settings.py

go to the `<default_app>/settings.py` and edit the `INSTALLED_APPS` list to include `<app_name>.apps.<CamelCaseAppNameConfig>`

3. MODIFY YOUR DEFAULT APP URLS.PY

	from django.urls import include

	path('/<your_prefix>', include('<app_name>.urls'))

4. CREATE A URLS.PY IN YOUR `<app_name>`

5. ADD A PATH TO YOUR VIEW FUNCTION IN `<app_name>/urls.py`

	from django.urls import path

	from . import views

	urlpatterns = [
    	path('/<your_suffix>', views.<your_function_without_()>, name='<url_name>')
	]

6. IN `<APP_NAME>/views.py` IMPLEMENT THE `views.<your_function>`

	from django.http import HttpResponse

	def your\_function(request):

    		<some python code here>

    		return HttpResponse("<your response here>")

7. Django's user authentication system is well-documented: https://docs.djangoproject.com/en/4.1/topics/auth/default/

