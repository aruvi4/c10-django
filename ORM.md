OOP - RDBMS
class - table
attributes - column
instance - row
object composition - foreign key

with this mapping, my job of getting data from the database becomes the act of instantiating the class with some search parameters (like a value for a primary key)

On the other side, if I have a fully specified instance, I should be able to store the values in the database by calling some method.

How to do this in Django?
Step 1: Create your model classes by extending `models.Model`
Step 2: Create your migrations by running `python3 manage.py makemigrations`
Step 3(optional): Ensure that your migrations look good by running `python3 manage.py sqlmigrate <appname> <migration number>`
Step 4: Run your migration by running `python3 manage.py migrate`. If it's the first time you're running it and you haven't changed the defaults, don't panic, it's also migrating some admin-related stuff that you can ignore for now.
Step 5: Test your migration by running `python3 manage.py shell` where you can import your model classes and instantiate them and do CRUD (Create-Read-Update-Delete).