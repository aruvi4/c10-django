from django.shortcuts import render
from django.http import HttpResponse
from .collatz import collatz
# Create your views here.


def get_collatz_sequence(request, start):
    sequence = collatz(start)
    standard_response = f'''
    <!DOCTYPE html>
        <html>

            <head>
                <title>Collatz</title>
            </head>

            <body>
                <h2>Collatz sequence for {start} </h2>
                <p>{sequence}</p>
            </body>

        </html>
        '''

    return HttpResponse(standard_response)

def get_collatz_req_params(request):
    start = int(request.GET.get('start', '1'))
    sequence = collatz(start)
    standard_response = f'''
    <!DOCTYPE html>
        <html>

            <head>
                <title>Collatz</title>
            </head>

            <body>
                <h2>Collatz sequence for {start} </h2>
                <p>{sequence}</p>
            </body>

        </html>
        '''

    return HttpResponse(standard_response)


def process_collatz_form(request):
    start = int(request.POST.get('start', '1'))
    sequence = collatz(start)
    context = {'start': start, 'sequence': sequence}
    return render(request, 'collatz/collatz.html', context)

def show_collatz_form(request):
    return render(request, 'collatz/collatz_form.html')