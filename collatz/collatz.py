from typing import List
def collatz(start: int) -> List[int]:
    if start == 4:
        return [4, 2, 1]
    if start == 2:
        return [2, 1, 4, 2, 1]
    if start == 1:
        return [1, 4, 2, 1]
    if start % 2 == 0:
        return [start] + collatz(start // 2)
    return [start] + collatz(start * 3 + 1)
