from django.urls import path
from . import views
urlpatterns = [
    path('<int:start>', views.get_collatz_sequence, name='collatz_sequence'),
    path('withparams', views.get_collatz_req_params, name='collatz_withparams'),
    path('processform', views.process_collatz_form, name='process_collatz_form'),
    path('form', views.show_collatz_form, name='show_collatz_form')
]