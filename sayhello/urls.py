from django.urls import path, include
from . import views

urlpatterns = [
    path('plain-str', views.say_hello, name='plain_say_hello'),
    path('html', views.say_hello_in_html, name='html_say_hello'),
    path('sayhello/<str:name>', views.say_hello_with_template, name='template_say_hello'),
    path('secret', views.show_secret_page, name='secret'),
    path('secret/password', views.process_password, name='secret_password'),
    path('secret/reset', views.reset_password, name='secret_reset')
]