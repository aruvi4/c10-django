from django.shortcuts import render, redirect
from django.http import HttpResponse

# Create your views here.

def say_hello(request):
    return HttpResponse("Hello world!")

def say_hello_in_html(request):
    return HttpResponse('''
    <!DOCTYPE html>
    <html>
        <head>
            <title>Hello!</title>
        </head>
        <body>
            <h2>Hello world!</h2>
            <p>You can send some HTML as a http response by simply passing it as a string argument to the HttpResponse init method.</p>
        </body>
    </html>
    ''')

def say_hello_with_template(request, name):
    age = int(request.GET.get('age', '16'))
    adult = age >= 18
    context = {'name': name, 'adult': adult}
    return render(request, 'sayhello/hello.html', context)

def show_secret_page(request):
    knows_password = request.session.get('is_authenticated', False)
    context = {'knows_password': knows_password}
    return render(request, 'sayhello/secret.html', context)

def process_password(request):
    secret_word = 'buttabomma'
    matches = request.POST.get('password', '') == secret_word
    request.session['is_authenticated'] = matches
    return redirect('secret')

def reset_password(request):
    del request.session['is_authenticated']
    return redirect('secret')
