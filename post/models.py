from django.db import models
from datetime import date, timedelta
# Create your models here.

def five_days_from_now():
    return date.today() + timedelta(days=5)


class Package (models.Model):
    address = models.CharField(max_length = 1000)
    phone = models.CharField(max_length = 10)
    status = models.CharField(max_length = 50, default='Received')
    eta = models.DateField(default=five_days_from_now)
