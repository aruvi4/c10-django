from django.shortcuts import render, redirect
from .models import Package
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponse

# Create your views here.


def register_package(request):
    if request.method == 'GET':
        return render(request, 'post/register_package.html')
    if request.method == 'POST':
        address_ = request.POST.get('address', '')
        phone_ = request.POST.get('phone', '')
        package = Package(address=address_, phone=phone_)
        package.save()
        context = {'success': True, 'id': package.id}
        return render(request, 'post/register_package.html', context)

def login_view(request):
    if request.method == 'POST':
        username_ = request.POST.get('username', '')
        password_ = request.POST.get('password', '')
        user = authenticate(request, username=username_, password=password_)
        if user is not None:
            login(request, user)
            return redirect('post:list_packages')
    return render(request, 'post/login.html')

@permission_required('post.view_package', login_url='post:login')
def package_list_view(request):
    packages = Package.objects.all()
    return render(request, 'post/list_packages.html', {'packages': packages})

@permission_required('post.change_package', login_url='post:login')
def modify_package(request, id):
    package = Package.objects.get(id=id)
    newstatus = request.POST.get('status', package.status)
    neweta = request.POST.get('eta', package.eta)
    package.status = newstatus
    package.eta = neweta
    package.save()
    return redirect('post:list_packages')

def package_detail(request, id):
    package = Package.objects.get(id=id)
    return render(request, 'post/package_detail.html', {'package': package})

def logout_view(request):
    logout(request)
    return redirect('post:login')
